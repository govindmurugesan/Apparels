$(document).ready(function () {
	$('.toggle').on('click', function() {
	  $('. container').stop().addClass('active');
	});

	$('.close').on('click', function() {
	  $('. container').stop().removeClass('active');
	});

    $(document).click( function(){
        $('.menu_sec').hide();
    });
    $('#bs-example-navbar-collapse-1').click( function(event){
        event.stopPropagation();
        $('.menu_sec').toggle();
    });
  
	
	$('.products').click(function() {
		selectProduct(this);
      $('.simulator .front_view').show();
      $('.simulator .left_view,.simulator .right_view,.simulator .back_view').hide();
	});
  $('.dropdown').click(function() {
    $(this).toggleClass('cart-90deg');
  });
  $('.menurem').click(function(){
    $('#slidrmv').removeClass('slide-in');
  });
  $('.menurem').click(function(){
    $('#bs-example-navbar-collapse-1').removeClass('in');
  });
  $('#testelement').draggable();   

  $('.simulator .left_view,.left_view').hide();
  $('.simulator .right_view,.right_view').hide();
  $('.simulator .back_view,.back_view').hide(); 
  $('#front').click(function(){
    $('.simulator .front_view,#testelement .front_view').show();
    $('.simulator .left_view,.simulator .right_view,.simulator .back_view').hide();
    $('#testelement .back_view,#testelement .left_view,#testelement .right_view').hide();
  });
  $('#left').click(function(){
    $('.simulator .left_view, #testelement .left_view').show();
    $('.simulator .front_view,.simulator .right_view,.simulator .back_view').hide();
    $('#testelement .front_view,#testelement .right_view, #testelement .back_view').hide();
  });
  $('#right').click(function(){
    $('.simulator .right_view,#testelement .right_view').show();
    $('.simulator .front_view,.simulator .left_view,.simulator .back_view').hide();
    $('#testelement .front_view,#testelement .left_view,#testelement .back_view').hide();
  });
  $('#back').click(function(){
    $('.simulator .back_view ,#testelement .back_view').show();
    $('.simulator .front_view,.simulator .left_view,.simulator .right_view').hide();
    $('#testelement .front_view,#testelement .left_view,#testelement .right_view').hide();
  });
});

function selectProduct(ele){
  $('#testelement').animate({
       "left": $("#draggable").data("left"),
       "top": $("#draggable").data("top")
  });
	$('#testelement').empty();
	$('#testelement').append($(ele).html());
}
